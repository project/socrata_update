<?php
/*
Module to define menu and admin configuration
        Copyright (C) 2017  Kyle Veldhuizen, Shravanthi Rajagopal and Manasi Chaudari, Public Disclosure Commission

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2 of the License, or
        (at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
*/
/**
 * Admin hook admin settings
 * Settings for establishing connection with socrata - available as global variables in drupal
 */
function socrata_update_admin_settings($form, &$form_state)
{

    /*
     * Status
     */
    $form['socrata_update_status'] = array(
        '#type' => 'fieldset',
        '#title' => 'Socrata Update Queue Status',
        '#description' => _socrata_update_get_status(),
    );

    $total = variable_get('socrata_update_total_processed', '0');
    $form['socrata_update_status']['total'] = array(
        '#type' => 'fieldset',
        '#title' => 'Total items processed',
        '#description' => ' ' . $total,
    );

    /*
     * Socrata credentials
     */
    $form['socrata_creds_con'] = array(
        '#type' => 'fieldset',
        '#title' => 'Socrata Credentials',
    );

    $form['socrata_creds_con']['socrata_update_global_enable'] = array(
        '#title' => 'Global Enable',
        '#type' => 'checkbox',
        '#default_value' => variable_get('socrata_update_global_enable', 0),
    );

    $form['socrata_creds_con']['socrata_update_domain'] = array(
        '#type' => 'textfield',
        '#title' => 'Domain',
        '#required' => true,
        '#default_value' => variable_get('socrata_update_domain', ''),
    );

    $form['socrata_creds_con']['socrata_update_username'] = array(
        '#type' => 'textfield',
        '#title' => 'Username',
        '#required' => true,
        '#default_value' => variable_get('socrata_update_username', ''),
    );

    $form['socrata_creds_con']['socrata_update_password'] = array(
        '#type' => 'textfield',
        '#title' => 'Password',
        '#required' => true,
        '#default_value' => variable_get('socrata_update_password', '')
    );

    $form['socrata_creds_con']['socrata_update_app_token'] = array(
        '#type' => 'textfield',
        '#title' => 'App Token',
        '#required' => true,
        '#default_value' => variable_get('socrata_update_app_token', ''),
    );

    return system_settings_form($form);
}

/**
 * Generates a string to show how many items are in the queue.
 *
 * @return string
 */
function _socrata_update_get_status()
{
    $query = db_select('queue')
        ->fields('queue')
        ->condition('name', 'socrata_update', '=')
        ->orderBy('created', 'asc');
    $query_results = $query->execute()->fetchAll();

    $total = count($query_results);

    $oldest = isset($query_results[0]->created) ? date('Y-m-d h:m:s a', $query_results[0]->created) : null;
    $newest = isset($query_results[$total - 1]->created) ? date('Y-m-d h:m:s a', $query_results[$total - 1]->created) : null;

    $text = "<strong>Total queues:</strong> $total<br /><strong>Newest:</strong> $newest<br /><strong>Oldest:</strong> $oldest";

    return $text;
}

/** Node tab  */
/**
 * node tab hook settings
 * Node automation settings - displays all the node settings that are already configured.
 * Node settings are obtained from the database table 'socrata_update'
 *
 * Form for adding new configuration is available at the bottom of this tab
 */
function socrata_update_node_settings($form, &$form_state)
{

    //drupal_set_message("")

    // Global disable
    if (!variable_get('socrata_update_global_enable', 0)) {
        drupal_set_message(t("Socrata Update queuing has been disabled by settings.php."), 'warning', FALSE);
        //return;
    }

    $form['global-description'] = array(
        '#type' => 'item',
        //'#title' => t('Important'),
        '#markup' => "The settings on this page are not to be changed without careful planing. 
        If you need to change these settings you need to make sure the site is in maintenance mode so no
        nodes will be queued and then manually run the socrata_update cron job to clear the queue of any pending items.",
    );

    $query = db_select('socrata_update', 'socrata_update')->fields('socrata_update');
    $result = $query->execute();
    while ($record = $result->fetchAssoc()) {
        $record_id = $record['configuration_id'];
        $form_name = 'socrata_' . $record_id;

        $form[$form_name] = array(
            '#type' => 'fieldset',
            '#title' => $record['node_type'],
            '#tree' => TRUE,
            '#collapsed' => TRUE,
            '#collapsible' => TRUE
        );

        $form[$form_name]['socrata_node_type_' . $record_id] = array(
            '#type' => 'textfield',
            '#title' => 'Content Type Machine Name',
            '#default_value' => $record['node_type'],
            '#description' => "The machine name of the content type that will be tracked for inserts, updates, and deletes."
        );

        $form[$form_name]['socrata_description_' . $record_id] = array(
            '#type' => 'textarea',
            '#title' => 'Description',
            '#default_value' => $record['description'],
            '#description' => "Internal only description of this node update job."

        );

        $form[$form_name]['socrata_on_' . $record_id] = array(
            '#type' => 'checkbox',
            '#title' => 'Enabled',
            '#default_value' => $record['enabled']
        );

        $form[$form_name]['socrata_dataset_id_' . $record_id] = array(
            '#type' => 'textfield',
            '#title' => 'Socrata Dataset id',
            '#default_value' => $record['dataset_id']
        );

        $form[$form_name]['socrata_mysql_view_' . $record_id] = array(
            '#type' => 'textarea',
            '#title' => 'MySQL view(s)',
            '#default_value' => $record['mysql_view'],
            '#description' => "Comma separated list of database views or table to pull the data from."
        );

        $form[$form_name]['socrata_nid_col_' . $record_id] = array(
            '#type' => 'textfield',
            '#title' => 'NID Column name in Socrata Dataset',
            '#default_value' => $record['nid_column_name'],
            '#description' => "The column in Socrata that contains the Drupal node id."
        );
        $form[$form_name]['socrata_primary_col_' . $record_id] = array(
            '#type' => 'textfield',
            '#title' => 'Primary Key Column name in Socrata Dataset',
            '#default_value' => $record['primary_key_name'],
            '#description' => "The column in socrata that is the row identifier for the dataset."
        );
        $form[$form_name]['socrata_delete_fieldset_' . $record_id] = array(
            '#type' => 'fieldset',
            '#title' => 'Delete this record',
            '#collapsed' => TRUE,
            '#collapsible' => TRUE

        );
        $form[$form_name]['socrata_delete_fieldset_' . $record_id]['socrata_delete_' . $record_id] = array(
            '#type' => 'checkbox',
            '#title' => 'Delete',
            '#default_value' => 0,
        );
    }

    /** Socrata New Configuration  */
    $form['socrata_new_config'] = array(
        '#type' => 'fieldset',
        '#title' => 'Add new configuration',
        '#tree' => TRUE,
    );

    $form['socrata_new_config']['socrata_node_type'] = array(
        '#type' => 'textfield',
        '#title' => 'Content Type Machine Name',
        '#description' => "The machine name of the content type that will be tracked for inserts, updates, and deletes."
    );

    $form['socrata_new_config']['socrata_description'] = array(
        '#type' => 'textarea',
        '#title' => 'Description',
        '#description' => "Internal only description of this node update job."
    );

    $form['socrata_new_config']['socrata_on'] = array(
        '#type' => 'checkbox',
        '#title' => 'Enabled',
        '#description' => ""
    );

    $form['socrata_new_config']['socrata_dataset_id'] = array(
        '#type' => 'textfield',
        '#title' => 'Socrata Dataset id',
    );

    $form['socrata_new_config']['socrata_mysql_view'] = array(
        '#type' => 'textarea',
        '#title' => 'MySQL view(s)',
        '#description' => "Comma separated list of database views or table to pull the data from."
    );

    $form['socrata_new_config']['socrata_nid_col'] = array(
        '#type' => 'textfield',
        '#title' => 'NID Column name in Socrata Dataset',
        '#description' => "The column in Socrata that contains the Drupal node id."
    );

    $form['socrata_new_config']['socrata_primary_col'] = array(
        '#type' => 'textfield',
        '#title' => 'Primary Key Column name in Socrata Dataset',
        '#description' => "The column in socrata that is the row identifier for the dataset."
    );


    $form['submit_button'] = array(
        '#type' => 'submit',
        '#value' => t('Save Configuration'),
    );

    return $form;
}

/**
 * @param $form
 * @param $form_state
 *
 * Validation for adding new node configuration
 */
function socrata_update_node_settings_validate($form, &$form_state)
{
    if (!empty($form_state['values']['socrata_new_config']['socrata_node_type']) || !empty($form_state['values']['socrata_new_config']['socrata_dataset_id']) || !empty($form_state['values']['socrata_new_config']['socrata_mysql_view']) || !empty($form_state['values']['socrata_new_config']['socrata_nid_col'])) {

        if (empty($form_state['values']['socrata_new_config']['socrata_node_type'])) {
            form_set_error('socrata_new_config', t('Node Type cannot be blank'));

        } else if (empty($form_state['values']['socrata_new_config']['socrata_dataset_id'])) {
            form_set_error('socrata_new_config', t('Dataset Id cannot be blank'));

        } else if (empty($form_state['values']['socrata_new_config']['socrata_mysql_view'])) {
            form_set_error('socrata_new_config', t('My SQL view cannot be blank'));

        } else if (empty($form_state['values']['socrata_new_config']['socrata_nid_col'])) {
            form_set_error('socrata_new_config', t('NID column name cannot be blank'));

        } else if (empty($form_state['values']['socrata_new_config']['socrata_primary_col'])) {
            form_set_error('socrata_new_config', t('Primary key column name cannot be blank'));

        }

        $query = db_select('socrata_update', 'socrata_update')->fields('socrata_update');
        $query->condition('node_type', $form_state['values']['socrata_new_config']['socrata_node_type'], '=');
        $query->condition('dataset_id', $form_state['values']['socrata_new_config']['socrata_dataset_id'], '=');
        $result = $query->execute();
        if ($record = $result->fetchAssoc()) {
            form_set_error('socrata_new_config', t('Node type and Dataset Id combination already exists. Please update in existing configuration'));
        }
    }

    $node_dataset = array();
    foreach ($form_state['values'] as $key => $row) {
        $matches = array();
        if (preg_match('/\d+/', $key, $matches)) {
            $record_name = $matches[0];

            if (in_array($row['socrata_node_type_' . $record_name] . '_' . $row['socrata_dataset_id_' . $record_name], $node_dataset)) {
                form_set_error('socrata_' . $record_name, t('Node type and Dataset Id combination already exists. Please update in existing configuration'));
            } else {
                $node_dataset[] = $row['socrata_node_type_' . $record_name] . '_' . $row['socrata_dataset_id_' . $record_name];
            }
        }
    }

}

/**
 * @param $form
 * @param $form_state
 *
 * On calling form submit, the existing configurations get updated on the database table.
 * New configuration gets inserted in the database table
 */
function socrata_update_node_settings_submit($form, &$form_state)
{
    if (!empty($form_state['values']['socrata_new_config']['socrata_node_type'])) {
        $p_id = db_insert('socrata_update')
            ->fields(array(
                'dataset_id' => $form_state['values']['socrata_new_config']['socrata_dataset_id'],
                'enabled' => $form_state['values']['socrata_new_config']['socrata_on'],
                'mysql_view' => $form_state['values']['socrata_new_config']['socrata_mysql_view'],
                'nid_column_name' => $form_state['values']['socrata_new_config']['socrata_nid_col'],
                'node_type' => $form_state['values']['socrata_new_config']['socrata_node_type'],
                'description' => $form_state['values']['socrata_new_config']['socrata_description'],
                'primary_key_name' => $form_state['values']['socrata_new_config']['socrata_primary_col']
            ))
            ->execute();

        $result = $p_id;
    }
    foreach ($form_state['values'] as $key => $row) {
        $matches = array();
        if (preg_match('/\d+/', $key, $matches)) {
            $record_name = $matches[0];

            if($row['socrata_delete_fieldset_' . $record_name]['socrata_delete_' . $record_name]){
                $num_deleted = db_delete('socrata_update')
                    ->condition('configuration_id', $record_name)
                    ->execute();
            }else{
                $num_updated = db_update('socrata_update')
                    ->fields(array(
                        'dataset_id' => $row['socrata_dataset_id_' . $record_name],
                        'enabled' => $row['socrata_on_' . $record_name],
                        'mysql_view' => $row['socrata_mysql_view_' . $record_name],
                        'nid_column_name' => $row['socrata_nid_col_' . $record_name],
                        'node_type' => $row['socrata_node_type_' . $record_name],
                        'description' => $row['socrata_description_' . $record_name],
                        'primary_key_name' => $row['socrata_primary_col_' . $record_name]
                    ))
                    ->condition('configuration_id', $record_name, '=')
                    ->execute();
            }
        }
    }
    drupal_set_message(t('The configuration options have been saved.'));
}