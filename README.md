
        This module is a plugin for forena reports that use Socrata as open data source
        Copyright (C) 2017  Kyle Veldhuizen, Shravanthi Rajagopal and Manasi Chaudari 
        Public Disclosure Commission
        

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2 of the License, or
        (at your option) any later version.
        

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
Socrata Update module

The modifications to various nodes need to be updated in the Socrata Datasets in real-time. This modules helps in automating the same by using drupal cron queues.

The modification details are passed into the inbuilt drupal queues. The various packets in the queue are then processed asynchronously by another thread based on the type of operation

There are 3 possible types of operation:
1. Insert - I
2. Update - U
3. Delete - D

Configuration setup:

This automation has to be configured and enabled in the Configuration -> Socrata Update page
The Socrata Update page has 2 settings:
- General - Socrata Credentials and App-token to connect with various datasets
- Node Updates - The nodes for which changes have to be reflected in Socrata Dataset are added here
   - Fields in Node Updates:
     For each unique combination of (Node Type and Dataset Id), one configuration has to be present. To configure a Node automation , following details are essential:
        - Machine Name of the node
        - Socrata Dataset Id
        - Corresponding Mysql view - Can have more than one view seperated by , (All views should belong to the same dataset)
        - NID column name in the dataset
        - Primary column name in the dataset
        - Enabled
        
Testing the module:

If the corresponding node configuration is setup and enabled, upon making changes to the node, we can run Cron and the changes will be reflected in the dataset



